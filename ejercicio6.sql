﻿DROP DATABASE IF EXISTS p1ej6;
CREATE DATABASE IF NOT EXISTS p1ej6;
USE p1ej6;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE compran(
  idclientes int,
  idproductos int,
  codigo int,
  fecha date,
  cantidad int,
  PRIMARY KEY(idclientes,idproductos,codigo)
);

CREATE OR REPLACE TABLE telefonos(
  idclientes int,
  telefono varchar(12),
  PRIMARY KEY(idclientes,telefono)
);

CREATE OR REPLACE TABLE comprandatos(
  idproductos int,
  idclientes int,
  fecha date,
  cantidad int,
  PRIMARY KEY(idproductos,idclientes,fecha)
);

CREATE OR REPLACE TABLE tienda(
  codigo int,
  direccion varchar(50),
  PRIMARY KEY(codigo)
);

ALTER TABLE compran
  ADD CONSTRAINT fkcompranproductos FOREIGN KEY(idproductos) REFERENCES productos(id),
  ADD CONSTRAINT fkcompranclientes FOREIGN KEY(idclientes) REFERENCES clientes(id),
  ADD CONSTRAINT fkcomprantiendas FOREIGN KEY(codigo) REFERENCES tienda(codigo),
  ADD CONSTRAINT ukcompran UNIQUE KEY(idproductos,codigo);

ALTER TABLE telefonos
  ADD CONSTRAINT fktelefonosclientes FOREIGN KEY(idclientes) REFERENCES clientes(id);

ALTER TABLE comprandatos
  ADD CONSTRAINT fkcomprandatoscomprar FOREIGN KEY(idclientes,idproductos) REFERENCES compran(idclientes,idproductos);